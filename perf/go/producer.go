package main
import (
	"bitbucket.org/swigy/kafka-client-go/swgykafka"
	"fmt"
	"github.com/google/uuid"
	"io/ioutil"
	"os"
	"strconv"
	"sync"
	"time"
)
var producer *swgykafka.Producer
var topic *swgykafka.Topic
var config *swgykafka.ProducerConfig
var clientId = "algates_producer"
var isSync = true
var isCompressed = true
var noOfThreads = 1
var isAck = true
var ack = swgykafka.AckAll
var wgProducer sync.WaitGroup
var filePath = "/tmp/1KB.json"
var producerDuration = 60
var noOfMessages = -1
var totalSuccessCount int64
var totalTriedCount int64
var totalErrorCount int64
var totalMessageCount int64
var isProducerDebug = false
var producerTopicName = "perftest_confluent"
var timeBeforeSend time.Time
var wgCallbacks sync.WaitGroup
var wgMessageCounts sync.WaitGroup
var messageCountChannel = make(chan string, 1000000)
var expectedProducedMessageCount = 1
var tolerancePercentage = 10
var faultStrategy = "FaultNone"
const (
	tried = "TriedCount"
	total = "TotalCount"
	error1 = "ErrorCount"
	success = "SuccessCount"
)
func main() {
	bootstrapServer := "pkc-lg991.us-east-1.aws.confluent.cloud:9092"
	username := "VNA2WFZCDEESAEZO"
	password := "0qOUtoP4UKJ1tdxgzoG71/Wq4jQZ0IOrW/r9I8cgi1z65qzbxja50CT5mf7iGRT1"
	Inhouse_cluster := "central-kafka-cluster-01.swiggyperf.in:9092,central-kafka-cluster-02.swiggyperf.in:9092,central-kafka-cluster-03.swiggyperf.in:9092,central-kafka-cluster-04.swiggyperf.in:9092,central-kafka-cluster-05.swiggyperf.in:9092,central-kafka-cluster-06.swiggyperf.in:9092"
	//Inhouse_cluster := "delivery-kafka-01.swiggyperf.in:9092,delivery-kafka-02.swiggyperf.in:9092,delivery-kafka-03.swiggyperf.in:9092,delivery-kafka-04.swiggyperf.in:9092,delivery-kafka-05.swiggyperf.in:9092,delivery-kafka-06.swiggyperf.in:9092,delivery-kafka-07.swiggyperf.in:9092,delivery-kafka-08.swiggyperf.in:9092"
	if len(os.Args) > 1 {
		noOfThreads,_ = strconv.Atoi(os.Args[1])
	}
	if len(os.Args) > 2 {
		isCompressed,_ = strconv.ParseBool(os.Args[2])
	}
	if len(os.Args) > 3 {
		isAck,_ = strconv.ParseBool(os.Args[3])
	}
	if len(os.Args) > 4 {
		filePath = os.Args[4]
	}
	if len(os.Args) > 5 {
		isSync,_ = strconv.ParseBool(os.Args[5])
	}
	if len(os.Args) > 6 {
		producerDuration,_ = strconv.Atoi(os.Args[6])
	}
	if len(os.Args) > 7 {
		noOfMessages,_ = strconv.Atoi(os.Args[7])
	}
	if len(os.Args) > 8 {
		producerTopicName  = os.Args[8]
	}
	if len(os.Args) > 9 {
		isProducerDebug,_ = strconv.ParseBool(os.Args[9])
	}
	if len(os.Args) > 10 {
		expectedProducedMessageCount,_ = strconv.Atoi(os.Args[10])
	}
	if len(os.Args) > 11 {
		tolerancePercentage,_ = strconv.Atoi(os.Args[11])
	}
	if len(os.Args) > 12 {
		faultStrategy = os.Args[12]
	}
	if !isAck {
		ack = swgykafka.AckNone
	}
	primary, _ := swgykafka.NewClusterBuilder(bootstrapServer).AuthMechanism(swgykafka.AuthSaslPlain).UserName(username).Password(password).Build()
	secondary, _ := swgykafka.NewClusterBuilder(Inhouse_cluster).AuthMechanism(swgykafka.AuthNone).Build()
	if faultStrategy == "FaultNone"{
		topic,_ = swgykafka.NewTopicBuilder(producerTopicName).FaultStrategy(swgykafka.FaultNone).Build()
		topicsMap := make(map[string]*swgykafka.Topic)
		topicsMap[producerTopicName] = topic
		//extraConfigs:=make(map[string]interface{})
		//extraConfigs["batch.num.messages"] = 1
		config, _ = swgykafka.NewProducerConfigBuilder(primary, nil, clientId).Topics(topicsMap).Retries(1).EnableCompression(isCompressed).Acks(ack).Build()
	} else{
		topic,_ = swgykafka.NewTopicBuilder(producerTopicName).FaultStrategy(swgykafka.FaultDualRW).Build()
		topicsMap := make(map[string]*swgykafka.Topic)
		topicsMap[producerTopicName] = topic
		//extraConfigs:=make(map[string]interface{})
		//extraConfigs["batch.num.messages"] = 1
		config, _ = swgykafka.NewProducerConfigBuilder(primary, secondary, clientId).Topics(topicsMap).Retries(1).EnableCompression(isCompressed).Acks(ack).Build()
	}
	var err error
	producer, err = swgykafka.NewProducer(*config)
	if err != nil {
		panic(err)
	}
	for i:=0; i<noOfThreads; i++ {
		wgProducer.Add(1)
		go producerWorker()
	}
	go messageCountWorker()
	wgProducer.Wait()
	wgCallbacks.Wait()
	wgMessageCounts.Wait()
	toleranceValue := float64(tolerancePercentage)/float64(100)
	expectedProducedMessageCount_tolerancevalue := float64(expectedProducedMessageCount) - toleranceValue*float64(expectedProducedMessageCount)
	fmt.Println("TriedCount -->" + strconv.FormatInt(totalTriedCount/int64(producerDuration),10))
	sucessCount := strconv.FormatInt(totalSuccessCount/int64(producerDuration),10)
	fmt.Println("SuccessCount -->" +sucessCount )
	fmt.Println("Expected Message count -->", expectedProducedMessageCount)
	if ((totalSuccessCount/int64(producerDuration)) > int64(expectedProducedMessageCount) || (totalSuccessCount/int64(producerDuration)) >= int64(expectedProducedMessageCount_tolerancevalue)) {
		fmt.Println("Testcase: PASSED")
	}else {
		fmt.Println("Testcase: FAILED")
	}
	fmt.Println("ErrorCount -->" + strconv.FormatInt(totalErrorCount/int64(producerDuration),10))
	fmt.Println("TotalCount -->" + strconv.FormatInt(totalMessageCount/int64(producerDuration),10))
}
func producerWorker() {
	var now = time.Now()
	var future = time.Now().Add(time.Second * time.Duration(producerDuration))
	jsonFile,err := os.Open(filePath)
	if err!= nil {
		panic(err)
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)
	byteValueStr := string(byteValue)
	var producerLimiter <-chan time.Time
	if noOfMessages > 0 {
		producerLimiter = getRateLimiter(int64(noOfMessages))
	} else {
		producerLimiter = nil
	}
	for now.Unix() < future.Unix() {
		if producerLimiter != nil {
			<-producerLimiter
		}
		timeBeforeSend=time.Now()
		wgMessageCounts.Add(1)
		messageCountChannel <- tried
		rc,err:=producer.Send(producerTopicName, uuid.New().String(), byteValueStr, nil)
		timeAfterSend:=time.Now()
		if isProducerDebug {
			fmt.Println(time.Now().Sub(timeBeforeSend))
		}
		wgMessageCounts.Add(1)
		messageCountChannel <- total
		if err != nil {
			fmt.Println("Error while sending" + err.Error())
			wgMessageCounts.Add(1)
			messageCountChannel <- error1
		}
		if err==nil && isSync {
			producerCallback(timeAfterSend,rc)
		} else if err==nil && !isSync {
			wgCallbacks.Add(1)
			go producerCallback(timeAfterSend,rc)
		}
		now = time.Now()
	}
	wgProducer.Done()
}
func producerCallback(now1 time.Time, rc chan *swgykafka.ProducerRecord) {
	record := <-rc
	if record != nil && record.Err != nil {
		wgMessageCounts.Add(1)
		messageCountChannel <- error1
	} else if record != nil && record.Err == nil {
		wgMessageCounts.Add(1)
		messageCountChannel <- success
	}
	if isProducerDebug {
		fmt.Println(time.Now().Sub(now1))
	}
	if !isSync {
		wgCallbacks.Done()
	}
}
func getRateLimiter(rate int64) <-chan time.Time{
	var output = int64(1e9 / rate)
	ratelimiter := time.NewTicker(time.Nanosecond * time.Duration(output)).C
	return ratelimiter
}
func messageCountWorker() {
	for {
		message := <- messageCountChannel
		if message == total {
			totalMessageCount += 1
		} else if message == tried {
			totalTriedCount += 1
		} else if message == success {
			totalSuccessCount += 1
		} else if message == error1 {
			totalErrorCount += 1
		}
		wgMessageCounts.Done()
	}
}
