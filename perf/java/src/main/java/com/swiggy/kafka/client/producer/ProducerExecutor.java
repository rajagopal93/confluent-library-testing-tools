package com.swiggy.kafka.client.producer;

import com.google.common.util.concurrent.RateLimiter;
import com.google.gson.Gson;
import com.swiggy.kafka.client.Application;
import com.swiggy.kafka.client.common.Env;
import com.swiggy.kafka.client.pojo.Message;
import com.swiggy.kafka.clients.Record;
import com.swiggy.kafka.clients.producer.CallbackFuture;
import com.swiggy.kafka.clients.producer.Producer;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProducerExecutor implements Runnable {

    long totalCount = 0;
    long successCount = 0;
    long errorCount = 0;

    private String topicName;
    private int duration;
    private int delay;
    private  String messagePath;
    private boolean isSync;
    private boolean isCallback;
    private int noOfMessages;
    private Producer producer;
    private String data;
    private Gson gson = new Gson();

    Logger logger = Logger.getLogger(ProducerExecutor.class.getName());

    public ProducerExecutor(Producer producer, String topicName, boolean isSync, boolean isCallback, int duration, int delay, String messagePath, int noOfMessages) {
        this.duration = duration;
        this.delay = delay;
        this.messagePath = messagePath;
        this.topicName = topicName;
        this.isSync = isSync;
        this.isCallback = isCallback;
        this.producer = producer;
        this.noOfMessages = noOfMessages;

        try {
            this.data = new String(Files.readAllBytes(Paths.get(messagePath)));
        } catch (Exception e) {
            System.out.println("File read error");
        }

        if (Application.argumentMap.get(Env.LOG_LEVEL).equals("ALL")){
            logger.setLevel(Level.ALL);
        } else {
            logger.setLevel(Level.INFO);
        }
    }

    @Override
    public void run() {
        long now = System.currentTimeMillis();
        long end = now + (duration * 1000);
        RateLimiter rateLimiter = null;
        if (noOfMessages > 0) {
           rateLimiter = RateLimiter.create(noOfMessages);
        }


        while (end>now) {
            if (rateLimiter != null) {
                rateLimiter.acquire();
            }
            try {
                Message message = Message.builder().id(UUID.randomUUID().toString())
                        .timestamp(System.currentTimeMillis())
                        .data(this.data)
                        .build();
                if (isSync) {
                    producer.send(topicName, gson.toJson(message)).get();
                    totalCount++;
                    successCount++;
                    logger.fine("Message produced in Sync mode: " + message);
                } else {
                    CallbackFuture<Record> future = producer.send(topicName, gson.toJson(message));
                    if (!isCallback) {
                        totalCount++;
                        successCount++;
                        logger.fine("Message produced in Async mode without callback");
                    } else {
                        future.callback(new BiConsumer<Record, Exception>() {
                            @Override
                            public void accept(Record record, Exception e) {
                                if (e == null) {
                                    totalCount++;
                                    successCount++;
                                    logger.fine("Message produced in Async mode with callback");
                                } else {
                                    totalCount++;
                                    errorCount++;
                                    logger.severe("Error in message production callback");
                                }
                            }
                        }, 150000, TimeUnit.MILLISECONDS);
                    }
                }
            } catch (Exception e) {
                totalCount++;
                errorCount++;
                logger.severe("Error in message production");
            }
            if (duration!= -1) {
                now = System.currentTimeMillis();
            }
        }
        SampleProducer.totalCount.addAndGet(totalCount);
        SampleProducer.successCount.addAndGet(successCount);
        SampleProducer.errorCount.addAndGet(errorCount);
        SampleProducer.completedCount.addAndGet(1);
    }
}