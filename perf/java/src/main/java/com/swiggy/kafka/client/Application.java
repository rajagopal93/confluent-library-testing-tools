package com.swiggy.kafka.client;

import com.swiggy.kafka.client.common.Env;
import com.swiggy.kafka.client.consumer.SampleConsumer;
import com.swiggy.kafka.client.producer.ProducerEnv;
import com.swiggy.kafka.client.producer.SampleProducer;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application {

    public static Map<String,String> argumentMap = new HashMap<>();
    private static final String PRODUCER = "producer";
    private static final String CONSUMER = "consumer";

    public static Logger logger = Logger.getLogger(Application.class.getName());

    public static void main(String args[]) throws InterruptedException {

        argumentMap.put(Env.APPLICATION_TYPE,PRODUCER);
        argumentMap.put(Env.LOG_LEVEL,"INFO");
        //Pushing default values into map
        argumentMap.put(ProducerEnv.PRIMARY_BOOTSTRAPSERVER,"pkc-lg991.us-east-1.aws.confluent.cloud:9092");
        argumentMap.put(ProducerEnv.PRIMARY_AUTH,"true");
        argumentMap.put(ProducerEnv.PRIMARY_USERNAME,"VNA2WFZCDEESAEZO");
        argumentMap.put(ProducerEnv.PRIMARY_PASSWORD,"0qOUtoP4UKJ1tdxgzoG71/Wq4jQZ0IOrW/r9I8cgi1z65qzbxja50CT5mf7iGRT1");

        argumentMap.put(ProducerEnv.SECONDARY_BOOTSTRAPSERVER,"");
        argumentMap.put(ProducerEnv.SECONDARY_AUTH,"true");
        argumentMap.put(ProducerEnv.SECONDARY_USERNAME,"");
        argumentMap.put(ProducerEnv.SECONDARY_PASSWORD,"");

        argumentMap.put(Env.TOPIC_NAME,"perf_golang");
        argumentMap.put(Env.FAULT_STRATEGY,"false");
        argumentMap.put(Env.RUN_DURATION,"0");
        argumentMap.put(Env.MAX_MESSAGES_TO_BE_PROCESSED,"0");
        argumentMap.put(Env.IS_REDIS_ENABLED,"false");
        argumentMap.put(Env.REDIS_HOST,"localhost");
        argumentMap.put(Env.REDIS_PORT,"6379");

        SampleProducer.loadProducerDefaults();
        SampleConsumer.loadConsumerDefaults();


        //Over writing values in map

        for (int i=0; i< args.length; i=i+2) {
            if (args[i].startsWith("--")) {
                Application.argumentMap.put(args[i], args[i + 1]);
            } else {
                System.out.println("Error in getting the arguments : " + args[0]);
            }
        }

        if (Application.argumentMap.get(Env.LOG_LEVEL).equals("ALL")){
            logger.setLevel(Level.ALL);
        } else {
            logger.setLevel(Level.INFO);
        }

        if (argumentMap.get(Env.APPLICATION_TYPE).equals(PRODUCER)) {
            logger.info("Starting app as a producer");
                SampleProducer.produce();
        } else if (argumentMap.get(Env.APPLICATION_TYPE).equals(CONSUMER)) {
            logger.info("Starting app as a consumer");
                SampleConsumer.consume();
        } else {
           logger.info("Please provide a valid application type by " + Env.APPLICATION_TYPE + " producer (or) consumer");
        }
    }

}
