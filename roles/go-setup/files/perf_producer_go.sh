#!/bin/bash
while read line;
do
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/
export LD_LIBRARY_PATH=/usr/local/lib/
cd /home/centos/goworkspace/src/bitbucket.org/swigy/kafka-client-go-perf-rg/
$line
echo "$line" >> /tmp/go/finalResults.out
grep -e 'TriedCount -->' -e 'SuccessCount -->' -e 'Expected Message count -->' -e 'ErrorCount -->' -e 'Testcase:' /tmp/go/results_perf.out | tail -5f >> /tmp/go/finalResults.out
done < /tmp/go/perftest_producer.txt
